'use strict';

timezonesApp.controller('LoginController', ['$rootScope', '$scope', 'loginService', function($rootScope, $scope, loginService) {
    var self = this;
    
    $scope.email = 'demo@toptal.com';
    $scope.password = 'toptal2016';
    
    $scope.login = function() {
    	loginService.logIn($scope.email, $scope.password).then(function(result) {
    		  var data = result.data
    		
			  if (data && data['access_token']) {
				console.log('User is logged in successfully');
				  
				localStorage['access_token'] = data['access_token'];
				localStorage['refresh_token'] = data['refresh_token'];
				
				$rootScope.onLogin(data['access_token']);
		      } else {
		    	$rootScope.logOut();
		      }
		 }, function(result) {
			  var error = result.data;
			  
			  $rootScope.logOut();
		      
		      swal("Error", error && error['error_description'] ? error['error_description'] : 'Error while logging in', "error");
		 });
    };
  }]);