'use strict';

timezonesApp.controller('TimezonesController', ['$scope', 'timezoneService', '$interval', '$location', function($scope, timezoneService, $interval, $location) {
    timezoneService.getTimezones().then(function(result) {
    	$scope.timezones = result.data.content;
    	
    	$scope.tickInterval = 1000; // ms
    	
        var tick = function() {
        	angular.forEach($scope.timezones, function(value, key) {
        		value.currentTime = Date.now();
        	});
        }
        
        tick();

        $interval(tick, $scope.tickInterval);
    }, function(result) {
    	console.log('Error while loading timezones: ' + result.data.error.message);
    });
    
    $scope.editTimezone = function(timezone) {
    	$location.path("/timezones/" + timezone.id);
    };
    
    $scope.removeTimezone = function(timezone) {
    	timezoneService.removeTimezone(timezone.id).then(function(result) {
        	var index = $scope.timezones.indexOf(timezone);
        	
        	if(index !== -1) {
        		$scope.timezones.splice(index, 1);
        	}
    	}, function(result) {
    		var error = result.data;
    		
    		swal("Error", error && error['error_description'] ? error['error_description'] : 'Error while removing timezone', "error");
    	});
    };
  }]);