'use strict';

timezonesApp.controller('AddOrUpdateTimezoneController', ['$scope', 'timezoneService', '$location', '$routeParams', function($scope, timezoneService, $location, $routeParams) {
    $scope.timezone = {};
    
    if($routeParams.id) {
    	timezoneService.getTimezone($routeParams.id).then(function(result) {
    		$scope.timezone = result.data;
    	}, function(result) {
    		swal("Error", 'Error while loading timezone #' + $routeParams.id, "error");
    	})
    }
	
	$scope.addOrUpdateTimezone = function() {
    	timezoneService.addOrUpdateTimezone($scope.timezone).then(function(result) {
    		$location.path("/timezones");
    	}, function(result) {
    		var error = result.data;
    		
    		var message = 'Error while saving timezone';
    		
    		if(error.status == 409) {
    			message = 'Provided city already exist';
    		} else if(error.status == 400) {
        		message = error.message;
    		}
    		// TODO: Handle validation
    		
    		swal("Error", message, "error");
    	});
    };
  }]);