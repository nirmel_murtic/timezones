 'use strict';

var timezonesApp = angular.module('timezonesApp', ['ngRoute', 'base64'])
	.constant('configuration', {
			'apiUrl': '',
			'clientId': 'web_client',
			'clientSecret': 'c58cc55d-54c3-43d9-9860-f387392bf7f9'
		}
	)
	.run(['configuration', '$rootScope', 'loginService', 'userService', '$route', '$location', '$http', 
	      function(configuration, $rootScope, loginService, userService, $route, $location, $http) {
			$rootScope.sideBarMenu = {
				'/timezones': {
					name: 'Timezones',
			    	active: true
				},
				'/addTimezone': {
					name: 'Add timezone',
				}
			};
			
			$rootScope.selectMenu = function(location) {
				angular.forEach($rootScope.sideBarMenu, function(value, key) {
					value.active = location.indexOf(key) !== -1;
				});
			};
		
			$rootScope.onLogin = function(token) {
				$rootScope.authenticated = true;
				
				$http.defaults.headers.common.Authorization = 'Bearer ' + token;

				if($route.current.$$route.originalPath == "/login") {
                    $location.path("/timezones");
                }
			};
			
			$rootScope.logIn = function() {
				$location.path("/login");
			};
			
			$rootScope.logOut = function() {
				$rootScope.authenticated = false;
				
				delete localStorage['access_token'];
				delete localStorage['refresh_token'];
				
				$location.path("/login");
			};
			
			$rootScope.$on("$routeChangeStart", function (event, next, current) {
	          if(current && !$rootScope.authenticated) {
	              if(next.$$route.originalPath != '/login') {
	                  $location.path( "/login" );
	              }
	          } else if($rootScope.authenticated && next.$$route && next.$$route.originalPath == '/login') {
	              event.preventDefault();
	          }
	          
	          if(next && next.$$route) {
	        	  $rootScope.selectMenu(next.$$route.originalPath);
	          }
	        });
			
			if(localStorage['access_token'] || localStorage['refresh_token']) {
				var refreshToken = function() {
					if(localStorage['refresh_token']) {
		    			 // If stored access token is not valid but refresh token exist, try to refresh token
						loginService.refreshToken(localStorage['refresh_token']).then(function(result) {
								var data = result.data;
								
		    		    	 	console.log('Access token is refreshed successfully, user is logged in.');
		    			        
		    					localStorage['access_token'] = data['access_token'];
		    					localStorage['refresh_token'] = data['refresh_token'];
		    					
		    					$rootScope.onLogin(data['access_token']);
		    		     }, function(result) {
		    		    	 	var error = result.data;
		    		    	 
		    		    	 	console.log('Error while refreshing token: ' + error);
		    		    	 	
		    		    	 	$rootScope.logOut();
		    		     });
		    		 } else {
		    			 $rootScope.logOut();
		    		 }
				};
				
				if(localStorage['access_token']) {
					// Check if stored access token is valid
					loginService.checkToken(localStorage['access_token']).then(function(result) {
						 var data = result.data; 
						
				    	 console.log('Stored access token is valid, user is logged in.');
				    	 
				    	 // If stored access token is valid, authenticate user
				    	 $rootScope.userInfo = data;
				    	 
				    	 $rootScope.onLogin(localStorage['access_token']);
				     }, function(error) {
				    	 console.log('Invalid access token, try to refresh it.');
				    	 
				    	 refreshToken();
				     });
				} else {
					refreshToken();
				}
			} else {
				$rootScope.logOut();
			}
		}
	])
	.config(['$routeProvider',
	      function($routeProvider) {
	        var user = function(userService, $rootScope, $interval, $q) {
	        	var deferred = $q.defer();
	        	
	        	var promise = $interval(function() {
	        		if(!$rootScope.authenticated) {
	        			return false;
	        		}
	        		
	        		$interval.cancel(promise);
	        		
	        		return userService.getCurrentUser().then(function(result){
						$rootScope.user = result.data;
						
						deferred.resolve($rootScope.user);
					}, function(result) {
						var error = result.data;
						
						deferred.reject(error && error['message'] ? error['message'] : 'Error while loading user');
						
						swal("Error while loading user", error && error['message'] ? error['message'] : '', "error")
						$rootScope.logOut();
					});
	        	}, 100);
	        	
	        	return deferred.promise;
	        }
		
	        $routeProvider.
	          when('/timezones', {
	            templateUrl: 'app/views/timezonesView.html',
	            controller: 'TimezonesController',
	            resolve: {
	            	user: user
	            }
	          }).
	          when('/timezones/:id', {
	        	templateUrl: 'app/views/addOrUpdateTimezoneView.html',
	            controller: 'AddOrUpdateTimezoneController',
	            resolve: {
	            	user: user
	            }
	          }).
	          when('/addTimezone', {
	            templateUrl: 'app/views/addOrUpdateTimezoneView.html',
	            controller: 'AddOrUpdateTimezoneController',
	            resolve: {
	            	user: user
	            }
	          }).
	          when('/login', {
	            templateUrl: 'app/views/loginView.html',
	            controller: 'LoginController'
	          }).
	          otherwise({
	            redirectTo: '/timezones'
	          });
	      }]);