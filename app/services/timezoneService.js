'use strict';

timezonesApp.factory('timezoneService', ['$http', 'configuration', function ($http, configuration) {
    return {
    	getTimezones: function() {
    		return $http({
    	        url: configuration.apiUrl + 'api/timezones', 
    	        method: "GET"
    	     });
    	},
    	getTimezone: function(id) {
    		return $http({
    	        url: configuration.apiUrl + 'api/timezones/' + id, 
    	        method: "GET"
    	     });
    	},
    	addOrUpdateTimezone: function(timezone) {
    		return $http({
    	        url: configuration.apiUrl + 'api/timezones', 
    	        data: {
    	        	name: timezone.name,
    	        	city: timezone.city,
    	        	offset: timezone.offset,
    	        	id: timezone.id
    	        },
    	        method: "POST"
    	     });
    	},
    	removeTimezone: function(id) {
    		return $http({
    	        url: configuration.apiUrl + 'api/timezones/' + id,
    	        method: "DELETE"
    	     });
    	}
    }
}]);