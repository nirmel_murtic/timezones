'use strict';

timezonesApp.factory('loginService', ['$http', '$base64', 'configuration', function ($http, $base64, configuration) {
    return {
    	logIn: function(email, password) {
    		return $http({
    	        url: configuration.apiUrl + 'oauth/token', 
    	        method: "POST",
    	        headers: {
    		    	authorization : 'Basic ' + $base64.encode(configuration.clientId + ':' + configuration.clientSecret)
    		    },
    	        params: {
    	        	grant_type: 'password',
    	        	username: email,
    	        	password: password
    	        }
    	     })
    	},
    	checkToken: function(token) {
    		return $http({
		        url: 'oauth/check_token', 
		        method: "GET",
		        params: {
		        	token: token
		        }
		     });
    	},
    	refreshToken: function(refreshToken) {
    		return $http({
		        url: 'oauth/token',
		        method: "POST",
		        headers: {
			    	authorization : 'Basic ' + $base64.encode(configuration.clientId + ':' + configuration.clientSecret)
			    },
		        params: {
		        	grant_type: 'refresh_token',
		        	refresh_token: refreshToken
		        }
		     });
    	}
    }
}]);