'use strict';

timezonesApp.factory('userService', ['$http', 'configuration', function ($http, configuration) {
    return {
    	getCurrentUser: function() {
    		return $http({
    	        url: configuration.apiUrl + 'api/users/currentUser', 
    	        method: "GET"
    	     });
    	}
    }
}]);