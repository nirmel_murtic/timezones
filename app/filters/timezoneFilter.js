'use strict';

timezonesApp.filter('timezone', function() {
    return function(value) {
    	var offset = value / 60;
    	
    	var time = offset.toString().split(".");
	    var hour = parseInt(time[0]);
	    var negative = hour < 0 ? true : false;
	    hour = Math.abs(hour) < 10 ? "0" + Math.abs(hour) : Math.abs(hour);
	    hour = negative ? "-" + hour : "+" + hour;
	    
	    var t = time[1]*6;
	    
	    if(time[1] >= 10) {
	    	t = t / 10;
	    }
	    
	    return time[1] ? hour+(t).toString() : hour + "00";
    };
  });
